package com.meghan.paymentconfigbusclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class PaymentconfigbusclientApplication {

	public static void main(String[] args) {
		SpringApplication.run(PaymentconfigbusclientApplication.class, args);
	}

}
